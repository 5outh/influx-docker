# Adapted from Ed's Vertex
FROM ubuntu:14.04
MAINTAINER Ben Kovach <bkovach5@uga.edu>

# Set environment to non-interactive
ENV DEBIAN_FRONTEND noninteractive

# Update the Ubuntu image
RUN apt-get update
RUN apt-get install -y software-properties-common python-software-properties
RUN add-apt-repository ppa:chris-lea/node.js
RUN add-apt-repository -y ppa:nginx/stable
RUN echo "deb http://us.archive.ubuntu.com/ubuntu/ precise universe" >> /etc/apt/sources.list
RUN apt-get update

## TOOLS
RUN apt-get install -y vim wget git curl

## INFLUXDB
RUN wget http://s3.amazonaws.com/influxdb/influxdb_latest_amd64.deb
RUN dpkg -i influxdb_latest_amd64.deb

## JAVA
## RUN sudo apt-get -y install openjdk-7-jdk

## START INFLUX
CMD /etc/init.d/influxdb start